Third-party library superbuild for the truchas-kernels project
--------------------------------------------------------------
This directory contains a CMake system for building most of the third party
libraries (TPL) needed by the truchas-kernels project.  You aren't required
to use this -- the TPLs can be built and installed manually as needed -- but
when it works, this provides a convenient method for automatically building
any TPLs not already available on your system.

### Quick Start Guide
The libraries that can be built are HDF5, NetCDF, Exodus, YAJL, and Petaca.
Compressed tarfiles of the source distributions can be found in the tarfiles
subdirectory.

The basic procedure is simple (when it works). You create a build directory,
run `cmake` from that directory, and then run `make`. What you choose for a
build directory is irrelevant, other than it can't be the directory containing
this README file. You will also provide CMake command line arguments that
customize the build in several ways. Here's an example:

    $ mkdir build
    $ cd build
    $ cmake -C ../config/intel.cmake ..
    $ make

The `-C` argument preloads the CMake cache with settings from the following
file.  The `config` subdirectory contains a few examples.  If none of those
are right for your situation, create your own, or simply define the various
variables directly on the cmake command line (using the `-D` flag).  By
default the packages are installed into the `install` subdirectory of the
build directory. Add `-D CMAKE_INSTALL_PREFIX=<tpl_dir>` to the CMake command
line to specify a different directory (replace `<tpl_dir>` with the directory
path; it must be an absolute path.)  Note that no `make install` command is
necessary; the libraries are automatically installed by the `make` command.
Also note that moving the libraries after they are installed will usually
break things, so it is best to decide where you want them before starting.

By default CMake will search for an existing installation of each library
and only configure a library build if it cannot find a suitable version.
You can disable this search and force the library to be built by setting
`-D SEARCH_FOR_<lib>=no` on the cmake command line.  Here `<lib>` can be
`HDF5`, `NETCDF`, `EXODUS`, `YAJL`, or `PETACA`. This is sometimes necessary
when CMake finds a library that you don't want it to use.  Also note that
when searching, CMake always looks first in the installation directory.

Another command line variable is `ENABLE_SHARED`.  Its default is `yes`.
Setting `ENABLE_SHARED` to `no`, meaning find and build static libraries,
generally doesn't work at the moment except on platforms that provide a "full"
set of static system libraries (Cray, for example).

### Package Configuration Notes
If you find you have to build some packages manually (or obtain pre-built
ones), here are some points to keep in mind.  Also look at the files in the
`cmake` directory to see how we are configuring them.

#### HDF5
* Only the C interface is needed.
* The high-level library (HL) is needed.
* Select a serial or MPI-parallel library as appropriate (`--enable-parallel`
  or `--disable-parallel`). The truchas-kernels project does not currently
  support MPI parallelism.

#### NetCDF
* Use `--with-netcdf-4`
* Only need the C interface (`--disable-fortran --disable-cxx`)
